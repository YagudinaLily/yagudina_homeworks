package Homework10;

public class Circle extends Ellipse implements MovableFigure{
    public MovableFigure movableFigure;
    public Circle(int x, int y, double radius) {
        super(x, y, radius, radius);
    }
    @Override
    public void moveTo (int x1, int y1) {
        x = x1;
        y = y1;
    }
}
