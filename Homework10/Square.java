package Homework10;

public class Square extends Rectangle implements MovableFigure {
    public MovableFigure movableFigure;
    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }
    @Override
    public void moveTo (int x1, int y1) {
        x = x1;
        y = y1;
    }
}
