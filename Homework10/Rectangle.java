package Homework10;

public class Rectangle extends Figure{
    private final int a;
    private final int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x, y);
        this.a = a;
        this.b = b;
    }
    @Override
    public double getPerimeter() {
        return a * 2 + b * 2;
    }
}
