package Homework10;

public class Ellipse extends Figure {
    private final double radius1;
    private final double radius2;

    public Ellipse(int x, int y, double radius1, double radius2) {
        super(x, y);
        this.radius1 = radius1;
        this.radius2 = radius2;
    }
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * Math.pow(((Math.pow(radius1, 2) + Math.pow(radius2, 2)) / 8), 0.5);
    }

}
