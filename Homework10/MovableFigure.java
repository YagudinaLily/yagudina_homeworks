package Homework10;

public interface MovableFigure {
    public void moveTo(int x1, int y1);
}
