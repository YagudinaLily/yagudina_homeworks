package Homework10;

public class Main {
    public static void main(String[] args) {
        MovableFigure circle = new Circle(88, 45, 22);
        MovableFigure circle1 = new Circle(45, 56, 67);
        MovableFigure square = new Square(12, 55, 22);
        MovableFigure square1 = new Square(77, 33, 88);
        MovableFigure square2 = new Square(67, 11, 35);

        MovableFigure[] movableFigures = {circle, circle1, square, square1, square2};

        for (MovableFigure movableFigure : movableFigures) {
            movableFigure.moveTo(55, 77);
        }
    }
}
